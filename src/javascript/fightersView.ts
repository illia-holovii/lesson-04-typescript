import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fighterService';

interface FighterGo {
    id: number|string;
    name: string;
    source: string;
    attack: number;
    defense: number;
    health: number;
}

class FightersView extends View {
  readyFighters: Array<string|number>;
  handleClick;
  createElement;
  element: HTMLElement;
  
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
    this.createStartGame();
    this.readyFighters = [];
  }

  fightersDetailsMap: Map<string|number, FighterGo> = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  handleFighterClick(event, fighter) {
    if (this.readyFighters.length < 2) {
      fighterService.getFighterDetails(fighter._id)
        .then((res) => this.fightersDetailsMap.set(fighter._id, res));
      setTimeout(this.editFightersSkills.bind(this), 1000, fighter._id);
      this.readyFighters.push(fighter._id);
      this.selectedPlayers(fighter);
    }
  }

  selectedPlayers(fighter) {
    const selectedDiv = this.createElement({ tagName: 'div', className: 'selected-fighter' });
    selectedDiv.innerHTML = `<p>PLAYER ${this.readyFighters.length} &#8594;</p> <img class="selected-fighter_img" src="${fighter.source}">`;
    this.element.append(selectedDiv);
    if (this.readyFighters.length === 2) {
      document.querySelector('.start-game').innerHTML = 'Start Game';
    }
  }

  editFightersSkills(id) {
    const modal = this.createElement({ tagName: 'div', className: 'fighter-details' });
    const nameFighters = this.createElement({ tagName: 'h3', className: 'fighter-details_name'});
    const healthFighters = this.createElement({ tagName: 'div', className: 'fighter-details_health'});
    const attackFighters = this.createElement({ tagName: 'div', className: 'fighter-details_attack'});
    const defenseFighters = this.createElement({ tagName: 'div', className: 'fighter-details_defense'});
    this.fightersDetailsMap.forEach( (value, key, map) => {
      if (key === id) {
        nameFighters.innerText = value.name;
        healthFighters.innerHTML = `<p>Health</p> <input type="text" class="edit-health" value="${value.health}">`;
        attackFighters.innerHTML = `<p>Attack</p> <input type="text" class="edit-attack" value="${value.attack}">`;
        defenseFighters.innerHTML = `<p>Defense</p> <input type="text" class="edit-defense" value="${value.defense}">`;
      }
    });
    const saves = this.createElement({ tagName: 'button', className: 'saves'});
    saves.innerText = 'SAVES';
    saves.addEventListener('click', () => this.editFighterSkills(id, modal), false);
    modal.append(nameFighters, healthFighters, attackFighters, defenseFighters, saves);
    this.element.append(modal);
  }

  editFighterSkills(id, modal) {
    this.fightersDetailsMap.forEach( (value, key, map) => {
      if (key === id) {
        const valueInputHealth: HTMLInputElement = document.querySelector('.edit-health');
        const valueInputAttack: HTMLInputElement = document.querySelector('.edit-attack');
        const valueInputDefense: HTMLInputElement = document.querySelector('.edit-defense');
        value.health = Number(valueInputHealth.value);
        value.attack = Number(valueInputAttack.value);
        value.defense = Number(valueInputDefense.value);
      }
    });
    this.element.removeChild(modal);
  }

  createStartGame() {
    const startGame = this.createElement({tagName: 'button', className: 'start-game'});
    startGame.innerText = 'Choose fighters (two) and wait a few seconds';
    startGame.addEventListener('click', () => this.chooseFighter(), false);
    this.element.append(startGame);
  }

  chooseFighter() {
    if (this.fightersDetailsMap.size === 2) {
      this.fight();
    }
  }

  fight() {
    const gameWindow = this.createElement({ tagName: 'div', className: 'game-window' });
    const gameInstruction = this.createElement({ tagName: 'h3', className: 'game-instruction' });
    gameInstruction.innerText = 'сlick on the fighter you want to hit';
    const fighterLeft = this.createElement({ tagName: 'div', className: 'fighter-left' });
    const fighterRight = this.createElement({ tagName: 'div', className: 'fighter-right' });
    this.fightersDetailsMap.forEach( (value, key, map) => {
      if (this.readyFighters[0] === key) {
        fighterLeft.innerHTML = `<h2>${value.name}</h2>
                                <img class="ready-fighters" src="${value.source}">
                                <div> <p>Attack</p> <p>${value.attack}</p> </div>
                                <div> <p>Defense</p> <p>${value.defense}</p> </div>
                                <div class="level-health"> <p>Health</p> <p id="left-fighter-health">${value.health}</p> </div>`;
      }
      if (this.readyFighters[1] === key) {
        fighterRight.innerHTML = `<h2>${value.name}</h2>
                                <img class="ready-fighters" src="${value.source}">
                                <div> <p>Attack</p> <p>${value.attack}</p> </div>
                                <div> <p>Defense</p> <p>${value.defense}</p> </div>
                                <div class="level-health"> <p>Health</p> <p id="left-fighter-health">${value.health}</p> </div>`;
      }
    })
    gameWindow.append(gameInstruction, fighterLeft, fighterRight);
    this.element.append(gameWindow);
    fighterLeft.addEventListener('click', () => this.strikeTheEnemy(0, 'left', gameWindow), false);
    fighterRight.addEventListener('click', () => this.strikeTheEnemy(1, 'right', gameWindow), false);
  }

  strikeTheEnemy(num, side, gameWindow) {
    const criticalHitChance = 1 + Math.random();
    const dodgeChance = 1 + Math.random();
    this.fightersDetailsMap.forEach( (value, key, map) => {
      if (this.readyFighters[num] === key) {
        value.health -= ((value.attack * criticalHitChance - value.defense * dodgeChance) > 0
        ? (value.attack * criticalHitChance - value.defense * dodgeChance) 
        : 0);
        const healthValue: string = String(value.health);
        document.getElementById(`${side}-fighter-health`).innerText = healthValue;
        if (value.health < 0) {
          this.fightersDetailsMap.forEach( (value, key, map) => {
            if (this.readyFighters[Number(!num)] === key) {
              gameWindow.innerHTML = `<h2>Game over</h2>
              <img id="close" class="ready-fighters" src="${value.source}">
              <p>WON ${value.name}</p>`;
              document.getElementById('close').addEventListener('click', () => this.close(gameWindow), false);
            }
          });
        }
      }
    });
  }

  close(gameWindow) {
    this.element.removeChild(gameWindow);
    this.fightersDetailsMap.clear();
    this.readyFighters.length = 0;
    const btnStartGame = document.querySelector('.start-game');
    btnStartGame.innerHTML = 'Choose fighters (two) and wait a few seconds';
    this.element.removeChild(document.querySelector('.selected-fighter'));
    this.element.removeChild(document.querySelector('.selected-fighter'));
  }
}

export default FightersView;
