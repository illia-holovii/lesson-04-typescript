class View {
  element: HTMLElement;

  createElement(args: { tagName: string, className: string, attributes: Object }) {
    const element = document.createElement(args.tagName);
    element.classList.add(args.className);
    
    const { attributes = {} } = args;
    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

    return element;
  }
}

export default View;