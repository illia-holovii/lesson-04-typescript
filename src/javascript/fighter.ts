import View from './view';
import FightersView from './fightersView';

class Fighter {
  attack: number;
  defense: number;
  health: number;
  name: string;
  id: number|string;
  source: string;
  constructor(fighter) {

    this.attack;
    this.defense;
    this.health;
    this.name = fighter.name;
    this.id = fighter._id;
    this.source = fighter.source;
  }
}

export default Fighter;
